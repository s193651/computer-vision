# -*- coding: utf-8 -*-
"""
@author: mxl
@date: 04/17/2020
"""
import nibabel as nib
from matplotlib import pyplot as plt
from nibabel.viewers import OrthoSlicer3D
import cv2
import glob
import numpy as np
import sys

img_file = glob.glob('dataset/3d_images/IMG_*.nii')
ch = ['A', 'B', 'C', 'D']
border = [(110, 430, 30, 500), (20, 440, 10, 495), (100, 480, 5, 512), (70, 420, 20, 500)]
for i in range(len(img_file)):
    data = nib.load(img_file[i])
    b = border[i]
    imset = data.get_fdata()
    for j in range(imset.shape[0]):
        img = imset[j,:,:]
        # thresholding
        foreground = -500
        th = cv2.threshold(img, foreground, 255, cv2.THRESH_BINARY_INV)[1]
        # crop
        m = np.zeros_like(th)
        m[b[0]:b[1],b[2]:b[3]] = 1
        th = m*th
        th = np.uint8(th)
        name = 'dataset/imgs/'+ch[i]+str((j)//100)+str((j-j//100*100)//10)+str(j%10)+'.tif'
        cv2.imwrite(name, th)
#img_file = glob.glob('dataset/3d_images/MASK_*.nii')
#for i in range(len(img_file)):
#    data = nib.load(img_file[i])
#    imset = data.get_fdata()
#    for j in range(imset.shape[0]):
#        img = imset[j,:,:]
#        img = np.uint8(img)
#        name = 'dataset/masks/'+ch[i]+str((j)//100)+str((j-j//100*100)//10)+str(j%10)+'.tif'
#        cv2.imwrite(name, img)

#width, height, queue = img.dataobj.shape
#print((width, height, queue))
#OrthoSlicer3D(img.dataobj).show()
