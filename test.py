import os
import numpy as np
import cv2
import matplotlib.pyplot as plt
from sklearn.model_selection import train_test_split
import glob

from keras.models import Model
from keras.layers import *
from keras.optimizers import Adam
from keras.regularizers import l2
from keras.preprocessing.image import ImageDataGenerator
import keras.backend as K
from keras.callbacks import LearningRateScheduler, ModelCheckpoint, EarlyStopping

# model
input_layer = Input(shape=(512, 512, 1))
c1 = Conv2D(filters=64, kernel_size=(3,3), activation='relu', padding='same')(input_layer)
c1 = Conv2D(filters=64, kernel_size=(3,3), activation='relu', padding='same')(c1)
l = MaxPool2D(strides=(2,2))(c1)
c2 = Conv2D(filters=128, kernel_size=(3,3), activation='relu', padding='same')(l)
c2 = Conv2D(filters=128, kernel_size=(3,3), activation='relu', padding='same')(c2)
l = MaxPool2D(strides=(2,2))(c2)
c3 = Conv2D(filters=256, kernel_size=(3,3), activation='relu', padding='same')(l)
c3 = Conv2D(filters=256, kernel_size=(3,3), activation='relu', padding='same')(c3)
l = MaxPool2D(strides=(2,2))(c3)
c4 = Conv2D(filters=512, kernel_size=(1,1), activation='relu', padding='same')(l)
c4 = Conv2D(filters=512, kernel_size=(1,1), activation='relu', padding='same')(c4)
l = MaxPool2D(strides=(2,2))(c4)
c5 = Conv2D(filters=1024, kernel_size=(1,1), activation='relu', padding='same')(l)
c5 = Conv2D(filters=1024, kernel_size=(1,1), activation='relu', padding='same')(c5)
l = MaxPool2D(strides=(2,2))(c5)
l = concatenate([UpSampling2D(size=(2,2))(c5), c4], axis=-1)
l = Conv2D(filters=256, kernel_size=(2,2), activation='relu', padding='same')(l)
l = Conv2D(filters=256, kernel_size=(2,2), activation='relu', padding='same')(l)
l = concatenate([UpSampling2D(size=(2,2))(l), c3], axis=-1)
l = Conv2D(filters=128, kernel_size=(2,2), activation='relu', padding='same')(l)
l = Conv2D(filters=128, kernel_size=(2,2), activation='relu', padding='same')(l)
l = concatenate([UpSampling2D(size=(2,2))(l), c2], axis=-1)
l = Conv2D(filters=64, kernel_size=(2,2), activation='relu', padding='same')(l)
l = Conv2D(filters=64, kernel_size=(2,2), activation='relu', padding='same')(l)
l = concatenate([UpSampling2D(size=(2,2))(l), c1], axis=-1)
l = Dropout(0.5)(l)
output_layer = Conv2D(filters=1, kernel_size=(1,1), activation='sigmoid')(l)
                                                         
model = Model(input_layer, output_layer)
#model.summary()

model.load_weights('lung.h5')

IMAGE_LIB = 'dataset/imgs/'
MASK_LIB = 'dataset/masks/'
img_file = glob.glob(IMAGE_LIB+'A*.tif')
mask_file = glob.glob(MASK_LIB+'A*.tif')
batch = 10
for i in range(len(img_file)):
    im = cv2.imread(img_file[i])[:,:,0]
    mask = cv2.imread(mask_file[i])
    im_pre = model.predict(im.reshape((1, im.shape[0], im.shape[1], 1)))[0,:,:,0]
    ero = cv2.erode(im_pre, kernel=np.ones((5, 5),np.uint8))
    ero = cv2.erode(im_pre, kernel=np.ones((7, 7),np.uint8))
    cv2.imwrite('dataset/split_imgs/'+'A'+str((i)//100)+str((i-i//100*100)//10)+str(i%10)+'.tif', ero)

